
/* Includes ------------------------------------------------------------------*/

#pragma once 

#include "stm32f0xx.h"


#define TFT_PORT	GPIOA
#define TFT_SPI		SPI1

#define PIN_LED		GPIO_PIN_1
#define PIN_RST		GPIO_PIN_3
#define PIN_DC		GPIO_PIN_2
#define PIN_CS		GPIO_PIN_4
#define PIN_SCK		GPIO_PIN_5
#define PIN_MISO	GPIO_PIN_6
#define PIN_MOSI	GPIO_PIN_7



#define ILI9340_TFTWIDTH  128
#define ILI9340_TFTHEIGHT 160

#define ILI9340_NOP     0x00
#define ILI9340_SWRESET 0x01
#define ILI9340_RDDID   0x04
#define ILI9340_RDDST   0x09

#define ILI9340_SLPIN   0x10
#define ILI9340_SLPOUT  0x11
#define ILI9340_PTLON   0x12
#define ILI9340_NORON   0x13

#define ILI9340_RDMODE  0x0A
#define ILI9340_RDMADCTL  0x0B
#define ILI9340_RDPIXFMT  0x0C
#define ILI9340_RDIMGFMT  0x0A
#define ILI9340_RDSELFDIAG  0x0F

#define ILI9340_INVOFF  0x20
#define ILI9340_INVON   0x21
#define ILI9340_GAMMASET 0x26
#define ILI9340_DISPOFF 0x28
#define ILI9340_DISPON  0x29

#define ILI9340_CASET   0x2A
#define ILI9340_PASET   0x2B
#define ILI9340_RAMWR   0x2C
#define ILI9340_RAMRD   0x2E

#define ILI9340_PTLAR   0x30
#define ILI9340_MADCTL  0x36


#define ILI9340_MADCTL_MY  0x80
#define ILI9340_MADCTL_MX  0x40
#define ILI9340_MADCTL_MV  0x20
#define ILI9340_MADCTL_ML  0x10
#define ILI9340_MADCTL_RGB 0x00
#define ILI9340_MADCTL_BGR 0x08
#define ILI9340_MADCTL_MH  0x04

#define ILI9340_PIXFMT  0x3A

#define ILI9340_FRMCTR1 0xB1
#define ILI9340_FRMCTR2 0xB2
#define ILI9340_FRMCTR3 0xB3
#define ILI9340_INVCTR  0xB4
#define ILI9340_DFUNCTR 0xB6

#define ILI9340_PWCTR1  0xC0
#define ILI9340_PWCTR2  0xC1
#define ILI9340_PWCTR3  0xC2
#define ILI9340_PWCTR4  0xC3
#define ILI9340_PWCTR5  0xC4
#define ILI9340_VMCTR1  0xC5
#define ILI9340_VMCTR2  0xC7

#define ILI9340_RDID1   0xDA
#define ILI9340_RDID2   0xDB
#define ILI9340_RDID3   0xDC
#define ILI9340_RDID4   0xDD

#define ILI9340_GMCTRP1 0xE0
#define ILI9340_GMCTRN1 0xE1
/*
#define ILI9340_PWCTR6  0xFC

*/

// Color definitions
#define	ILI9340_BLACK   0x0000
#define	ILI9340_BLUE    0xf800
#define	ILI9340_RED     0x001f
#define	ILI9340_GREEN   0x07e0
#define ILI9340_CYAN    0xffe0
#define ILI9340_MAGENTA 0xF81F
#define ILI9340_YELLOW  0x07ff
#define ILI9340_WHITE   0xFFFF
#define ILI9340_BRIGHT_BLUE	0xf810
#define ILI9340_GRAY1		0x8410
#define ILI9340_GRAY2		0x4208

#define TFT_CS_LOW		GPIO_WriteBit(TFT_PORT, PIN_CS, Bit_RESET)
#define TFT_CS_HIGH		GPIO_WriteBit(TFT_PORT, PIN_CS, Bit_SET)
#define TFT_DC_LOW		GPIO_WriteBit(TFT_PORT, PIN_DC, Bit_RESET)
#define TFT_DC_HIGH		GPIO_WriteBit(TFT_PORT, PIN_DC, Bit_SET)
#define TFT_RST_LOW		GPIO_WriteBit(TFT_PORT, PIN_RST, Bit_RESET)
#define TFT_RST_HIGH	GPIO_WriteBit(TFT_PORT, PIN_RST, Bit_SET)

void tft_fillScreen(unsigned short color);
void TFT_sendByte(uint8_t data);
void TFT_sendCMD(int index);
void TFT_sendDATA(int data);
void TFT_sendWord(int data);
int TFT_Read_Register(int Addr, int xParameter);
int TFT_readID(void);
void TFT_init();
void TFT_led(int state);
void TFT_setCol(int StartCol, int EndCol);
void TFT_setPage(int StartPage, int EndPage);
void TFT_setXY(int poX, int poY);
void TFT_setPixel(int poX, int poY, int color);
void fillScreen(void);

void tft_drawPixel(short x, short y, unsigned short color);
void tft_drawFastHLine(short x, short y, short w, unsigned short color);
void tft_drawFastVLine(short x, short y, short h, unsigned short color);
void tft_fillRect(short x, short y, short w, short h,
  unsigned short color);


